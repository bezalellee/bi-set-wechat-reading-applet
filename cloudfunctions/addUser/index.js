// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()

    // 先获取到所有用户数据
    // 将是创建的账号与这些数据对比，判断是否已经被创建
    // 使用async await后就不需要给get写回调函数了，
    let data = await db.collection('booklist').get()
    let isReg = false;
    
    data.data[1].userList.forEach(item => {
        if(item.username == event.username) {
            isReg= false;
        }else {
            isReg = true;
        }
    })
    // isReg = true 时像数据库添加数据
    // 单独使用add添加数据不能使用doc
    const userInfo = {
        appid:wxContext.APPID,
        avatarUrl:"",
        bookshelf:[],
        loading:false,
        nickName:"",
        openid:wxContext.OPENID,
        password:event.password,
        username:event.username,
        token:""
    }
    if(isReg) {
        // 以updata的方式添加信息
        db.collection('booklist').doc('5464a294625cdd7400bbd7b121407efa')
        .update({
            data:{
                // 通过db.command 的push方法将数据以对象的形式添加数据
                'userList':_.push(userInfo)
            }
        })
    }

    return {
        event,
        isReg,
        data
    }
}