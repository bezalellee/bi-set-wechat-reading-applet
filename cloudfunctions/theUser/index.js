// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({env: cloud.DYNAMIC_CURRENT_ENV})
const db = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()
    let returnValue = {};

    const userList = await db.collection('booklist').get()

    userList.data[1].userList.forEach(item => {
        if(item.username == event.username && item.password == event.password) {
            returnValue = item
        }
    })
    returnValue.loading = true

    return {
        event,
        openid: wxContext.OPENID,
        appid: wxContext.APPID,
        unionid: wxContext.UNIONID,
        returnValue,
        userList
    }
}