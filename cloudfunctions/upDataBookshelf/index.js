// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
async function getData() {
    return await db.collection('booklist').get()
}
// getData().then(res => {
//     console.log(res)
// })

// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext()

    let returnInfos = 123;

    // const data = await getData();
    data = await db.collection('booklist').get()
    
    data.data[1].userList.forEach(item =>{
        if(item.openid == event.openid) {
            returnInfos = item
        }
    })

    // 通过doc获取先要数据的父级所包含的说有数据，（测试后发现无法和where一起使用）
    // const a = await db.collection('booklist').doc('5464a294625cdd7400bbd7b121407efa').get()

    // 通过where条件筛选数据，条件可以有多个
    db.collection('booklist')
    .where({
        // "userList.$.openid":event.openid,
        // key值可以是字符串，也可以是对象类型的字符串，如下
        '_id':'5464a294625cdd7400bbd7b121407efa',
        'userList.openid':event.openid
    })
    // .get()
    .update({
        // 传入需要局部更新的数据
        data:{
            // 通过条件筛选过数据后
            // userList 是一个数组
            // userList.$ 代表所筛选的数组
            // userList.$.appid --》表示所筛选到的数据中的appid将被赋值
            'userList.$.appid':returnInfos.appid,
            'userList.$.username':returnInfos.username,
            'userList.$.password':returnInfos.password,
            'userList.$.openid':event.openid,
            'userList.$.avatarUrl':event.userInfo.avatarUrl,
            'userList.$.loading':event.userInfo.loading,
            'userList.$.nickName':event.userInfo.nickName,
            'userList.$.token':event.userInfo.token,
            'userList.$.bookshelf':event.bookshelf,
        }
    })

    return {
        event,
        openid: wxContext.OPENID,
        appid: wxContext.APPID,
        unionid: wxContext.UNIONID,
        returnInfos
    }
}