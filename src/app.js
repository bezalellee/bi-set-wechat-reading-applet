//app.js 

var https = require('utils/https.js')
App({
  // 使用云函数的配置
  onLaunch: function () {
    const that = this;
		if (!wx.cloud) {
		  console.error('请使用 2.2.3 或以上的基础库以使用云能力')
		} else {
		  wx.cloud.init({
			env: "cloud1-1gkd1emlef4f6146",//环境id
			traceUser: true,
		  })
    }

	  },

  globalData: {
    // domain: "https://www.zinglizingli.xyz",
    token:null,
    userInfo: null,
    userInfo: null,
    bookshelfChanged: false,
    statusBarHeight: 0,
    titleBarHeight: 0,
    wechatUser:null
  },

  trySetUserInfo: function () {
    var userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      this.globalData.userInfo = userInfo;
    }
  },
  removeCache:function(){
    var value = wx.getStorageSync('cateListTime')
    var nowTime = new Date().getTime();
    if(value){
      if (value < nowTime) {
        wx.removeStorage("cateList");
      }
    }
    value = wx.getStorageSync('searchKeysTime')
    if (value) {
      if (value < nowTime) {
        wx.removeStorage("searchKeysTime");
      }
    }
  }
})