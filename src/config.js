

// 是否是调试模式。如果是生产环境，请设置为false
const debug = false

// 横幅图片比例，宽高比，小数或者比例(后期从后端接口获取)
const bannerRatio = 825/315

const info = {
  about: 'help/bookchat',
  version: 'v1.0',
  author: 'TruthHun',
  license: 'Apache 2.0',
  copyright: ''
}

// 横幅广告id，如果申请了腾讯小程序的广告，则创建一个横幅广告，把广告的AdUnitId粘贴进来即可，不投放广告，则把该值设置为空
const bannerAdUnitId = ''


module.exports = {
  debug,
  bannerRatio,
  bannerAdUnitId,
  info,
}
