var app = getApp()

Page({
  data: {
    // text:"这是一个页面"
    list: [],
    deletList: [],
    windowHeight: 0,//获取屏幕高度
    refreshing:false,
    size: 20,
    canDeleteItemColor:'',
    isShow:false,
    isShowHis:false,
    history:[]
  },
// 历史记录
  openHistory:function(){
    this.setData({isShowHis:!this.data.isShowHis})
  },

  // 
  initWindowHeight:function(){
    //获取屏幕高度  
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight
        })
      }
    })
  },
  openManage:function() {
    this.setData({isShow:!this.data.isShow})
  },
  selectItems:function(e) {
    
    // 将展示列表中要删除项移除
    const newList = this.data.list
    const deletInfo = newList.splice(e.currentTarget.dataset.index,1)
    this.setData({list:newList})
    // 将要删除项，添加到删除列表
    const deletList = this.data.deletList
    deletList.push(deletInfo[0])
    // console.log(deletList)
    this.setData({deletList:deletList})
    // 删除storage中对应数据
    // wx.setStoragesetStorage({
    //   key:"booklist",
    //   data:this.data.list
    // })
    
  },
  reSelectItems:function(e) {
    // 将展示列表中要删除项移除
    const newDeletList = this.data.deletList
    const addInfo = newDeletList.splice(e.currentTarget.dataset.index,1)
    this.setData({deletList:newDeletList})
    // 将要删除项，添加到删除列表
    const newList = this.data.list
    newList.push(addInfo[0])
    this.setData({list:newList})
    // 删除storage中对应数据
    // wx.setStoragesetStorage({
    //   key:"booklist",
    //   data:this.data.list
    // })
    
  },
  selectAll:function() {
    // 将删除列表和展示列表合并，并赋值给删除列表
    const deletList = this.data.deletList;
    const list = this.data.list;
    list.forEach(item => {
      deletList.push(item)
    })
    this.setData({
      list:[],
      deletList
    })
  },
  selectNone:function() {
    // 将删除列表和展示列表合并，并赋值给展示列表
    const deletList = this.data.deletList;
    const list = this.data.list;
    deletList.forEach(item => {
      list.push(item)
    })
    this.setData({
      list,
      deletList:[]
    })
  },
  backtoind:function(){
    this.setData({isShowHis:!this.data.isShowHis})
  },
  backAndSyncData:function() {
    this.setData({isShow:!this.data.isShow})

    // 同步数据
    wx.setStorage({
      key:"booklist",
      data:this.data.list
    })
  },
  init:function(){
    var that = this;
    // 获取书籍列表
    wx.getStorageInfo({
      success: function (res) {
        let data = new Array();
        res.keys.forEach(function (item, index) {
          // if (item.startsWith("book")) {
          if (item.startsWith("book")) {
            var value = wx.getStorageSync(item);
            if (value) {
              data.unshift(value);
            }
          }
        });
        // 将获取到的列表添加到list属性中
        that.setData({
          list: data[0],
          hidden: true,
        });
      }
    })
  },
  onLoad: function (options) {
    this.openManage();
    var that = this;
    that.initWindowHeight();
    // wx.clearStorage()
    // that.init();
   
  },

  onReady: function () {
    // 页面渲染完成
   
  },
  onShow: function () {
    const that = this
    // 页面显示
    // 初始化
    this.init();
    this.setData({deletList:[]})
    
    wx.getStorage({
      key:'history',
      success(res){
        // console.log(res.data)
        that.setData({history:res.data})
      }
    })

    // 将书架中的数据和本地存储中的数据比较并更新
    wx.getStorage({
      key:'user',
      success(res) {
        // console.log(res.data,'===========')
        // console.log(that.data.list,'===========')
        let bookshelf = res.data.bookshelf || []
        bookshelf.forEach(item =>{
          // 没有对项，返回-1
          let index = that.data.list.findIndex((it) => it.id ==item.id)
          if(index == -1) {
            that.setData({list:that.data.list.push(item)})
          }
        })
        // 将书架更新的数据与本地同步（覆盖本地数据）
        bookshelf = that.data.list
        if(!bookshelf) {
          res.data.bookshelf = []
        }else {
          res.data.bookshelf = bookshelf
        }
        wx.setStorage({
          key:'user',
          data:res.data
        })
      }
    })


    
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },

  refesh:function(e){
    var that=this;
     //NewsChaptersUrl
    if (that.data.list && that.data.list.length>0){
      if (that.data.refreshing) {
        return;
      }
      that.setData({
        refreshing: true
      });
     
    }
    
  },
  //点击事件处理
  clickDetail: function (e) {
    wx.navigateTo({
      url: `../reader/reader?id=${e.currentTarget.dataset.id}&title=${e.currentTarget.dataset.title}`
    });
  },
  clickAddBook:function(){
    wx.switchTab({
      url: `../index/index`
    });
  },
  removeBookshelf:function(e)
  {
    var that = this;
    wx.showModal({
      title: '',
      content: '确定移除书架吗？',
      success: function (res) {
        if (res.confirm) {
          wx.removeStorageSync("book" + e.currentTarget.dataset.id);
          that.init();
        } 
      }
    })
  }
})