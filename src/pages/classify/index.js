// pages/classify/index.js
Page({

    data: {
        // text:"这是一个页面"
        list: [],
        windowHeight: 0, //获取屏幕高度
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 500,
        toView: "red",
        keyword: "",
        dotCurrent: 0,
        showUploadTip: false,
        haveGetRecord: false,
        envId: '',
        record: '',
        booklist:[],
        newlist: [],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: async function (option) {
        const that = this;
        //获取屏幕高度
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight
                })
            }
        })

        const data = await wx.cloud.callFunction({
            name: 'getBooks',
          })
            // 获取书籍信息
            this.setData({
              booklist: data.result.books.data[0].booklist
            });
            this.showList()

    },
    clickDetail: function (e) {
        wx.navigateTo({
            url: `../detail/detail?id=${e.currentTarget.dataset.id}&category=${e.currentTarget.dataset.category}&title=${e.currentTarget.dataset.title}`
        });
    },
    showList(e){
       
        if(!e){
            this.getSto();
        }else {
            switch (e.currentTarget.dataset.title) {
                case '推荐':
                    this.getSto();
                    break;
                case '男频':
                    this.getSto(10,20);
                    break;
                case '女频':
                    this.getSto(15,25);
                    break;
                case '最新上架':
                    this.getSto(5,15);
                    break;
            }
        }
        
        

    },

    getSto:function(start =0,end){
        end = end || this.data.booklist.length
        let clickData = [];
        for(let i =start;i<end;i++) {
            // console.log(this.data.booklist[i])
            clickData.push(this.data.booklist[i])
        }
            // 获取书籍信息
            this.setData({
                newlist: clickData
            });
    },

    
    onReady: function () {

    },

    
    onShow: function () {

    },

    onHide: function () {

    },

    onUnload: function () {

    },

    onPullDownRefresh: function () {

    },

    onReachBottom: function () {

    },

    onShareAppMessage: function () {

    }
})