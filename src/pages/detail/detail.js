var app = getApp()
var https = require('../../utils/https.js')
var util = require('../../utils/util.js')

wx.cloud.init();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        id: 0,
        visible: false,
        scrolly: true,
        info:[],
        showUploadTip: false,
        haveGetRecord: false,
        envId: '',
        record: '',
        item:{},
        showCatelog: false,
        catelogs: {},
        recList: {},
        inBookshelf: false,
        showMoreDesc: false,
        showMoreText: '更多介绍',
        windowHeight: 0,//获取屏幕高度
        title: ''
    },
    initWindowHeight: function () {
        //获取屏幕高度
        var that = this;
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight
                })
            }
        })
    },
    showDesc: function (e) {
        this.setData({
            showMoreDesc: !this.data.showMoreDesc,
            showMoreText: this.data.showMoreDesc ? '更多介绍' : '收起介绍'
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: async function (option) {
        
        let that = this;
        //   wx.showLoading({
        //     title: '',
        //   });
          const data = await wx.cloud.callFunction({
            name: 'getBooks',
          })
          let clickData = {};
            // 获取书籍信息
            data.result.books.data[0].booklist.forEach(items => {
                if (items.id === option.id) {
                    clickData = items
                }
            });
            this.setData({
              record : data.result.books.data[0].booklist,
              recList:clickData.recommend,
              item :clickData,
              visible :true,
              id : option.id,
              inBookshelf: inBookshelf
            });
            wx.setNavigationBarTitle({
            title: option.title
        });

        that.setData({
            title: option.title
        });

        that.initWindowHeight();
        var inBookshelf = false;
        // 
        if (wx.getStorageSync("book" + option.id)) {
            inBookshelf = true;
        }
        
        

    },
    
    refreshCatelog: function () {
        var that = this;
        that.initCatelog(that.data.id);
    },
    sortCatelog: function () {
        var that = this;
        var catelogs = that.data.catelogs;
        catelogs.text = catelogs.text.reverse();
        that.setData({
            catelogs: catelogs
        });
    },
    initCatelog: function (nid) {
        var that = this;
        let url = app.globalData.catalogUrl + `?nid=${nid}`
        wx.showLoading({
            title: '加载中',
            mark: true
        });
            wx.hideLoading();
    },
    //点击事件处理
    clickDetail: function (e) {
        wx.redirectTo({
            url: `./detail?id=${e.currentTarget.dataset.id}&category=${e.currentTarget.dataset.category}&title=${e.currentTarget.dataset.title}`
        });
    },
    changeShowCatelog: function (e) {
        var that = this;
        // 设置下是否显示
        that.setData({
            showCatelog: !that.data.showCatelog,
            scrolly: that.data.showCatelog
        })
        if (that.data.showCatelog) {
            that.setData({
                catelogs: that.data.item
            })
        }
    },
    toReader: function (e) {
        var that = this;
        let serialNumber = 1;
        if (e.currentTarget.dataset.serialnumber) {
            serialNumber = parseInt(e.currentTarget.dataset.serialnumber);
        }
        wx.navigateTo({
          url: `../reader/reader?id=${that.data.id}&serialNumber=${serialNumber}&title=${that.data.item.name}`
        });
       
    },
    insertBookShelf: function () {
        var that = this;
        var item = that.data.item;

        this.setData({info:wx.getStorageSync('booklist')})

        let insertInfo = this.data.info || []
        // 判断是否重复添加
        const index = insertInfo.findIndex((element) => {
            return element.id == item.id
        })
        if(index !== -1) {
            insertInfo.splice(index,1)
        }
        insertInfo.push(item)
        this.setData({info:insertInfo})

        wx.setStorage({
            key: "booklist",
            data: this.data.info,
            success: function () {
                wx.showToast({
                    title: '已加入书架',
                })
                that.setData({
                    inBookshelf: true
                })
            }
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        const that = this;
        
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        var that = this;
        return {
            title: `${that.data.title}`,
            path: `pages/detail/detail?id=${that.data.id}&title=${that.data.title}`,
            imageUrl: '',
            success: function (shareTickets) {
                console.info(shareTickets + '成功');
                // 转发成功
            },
            fail: function (res) {
                console.log(res + '失败');
                // 转发失败
            },
            complete: function (res) {
                // 不管成功失败都会执行
            }
        }
    }
})