var app = getApp()
var https = require('../../utils/https.js')


const order = ['red', 'yellow', 'blue', 'green', 'red']

Page({
    data: {
        // text:"这是一个页面"
        list: [],
        windowHeight: 0, //获取屏幕高度
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 500,
        toView: "red",
        swiperList:[],
        bookshelf:[],
        openid:'',
        keyword: "",
        dotCurrent: 0,
        showUploadTip: false,
        haveGetRecord: false,
        envId: '',
        record: '',
        newlist: [{
            "image": "https://xs.23sk.com/files/article/image/57/57829/57829s.jpg",
            "name": "平平无奇大师兄",
            "id": 26,
            "category": 1
        }, {
            "image": "https://xs.23sk.com/files/article/image/43/43362/43362s.jpg",
            "name": "轮回乐园",
            "id": 27,
            "category": 1
        }, {
            "image": "https://xs.23sk.com/files/article/image/60/60011/60011s.jpg",
            "name": "三国之曹家逆子",
            "id": 28,
            "category": 1
        }],
        hotlist: [{
            "image": "https://xs.23sk.com/files/article/image/5/5986/5986s.jpg",
            "name": "斗罗大陆IV终极斗罗",
            "id": 25,
            "category": 1,
            "author": "唐家三少",
            "desc": "一万年后，冰化了。　　斗罗联邦科考队在极北之地科考时发现了一个有着金银双色花纹的蛋，用仪器探察之后，发现里面居然有生命体征，赶忙将其带回研究所进行孵化。蛋孵化出来了，可孵出来的却是一个婴儿，和人类一模一样的婴儿，一个蛋生的孩子。"
        }, {
            "image": "https://xs.23sk.com/files/article/image/60/60224/60224s.jpg",
            "name": "左道倾天",
            "id": 4,
            "category": 1,
            "author": "风凌天下",
            "desc": "是非谁来判定，功过谁予置评？此生不想规矩，只求随心所欲。天机握在手中，看我飞扬跋扈。————我是左小多，我不走寻常路。"
        }, {
            "image": "https://xs.23sk.com/files/article/image/25/25704/25704s.jpg",
            "name": "龙纹战神",
            "id": 5,
            "category": 1,
            "author": "苏月夕",
            "desc": "天下第一圣重生百年后，修无上神功，争霸天下。别跟我比炼丹，十成丹随手就来。别跟我比晋级速度，羞死你我可不负责。别跟我比修炼经验，我是老祖。江尘的存在，注定要羞煞万千天才……【三本完本记录，人品值饱满，群：102827635】分享书籍《龙纹战神》作者：苏月夕"
        }],
        reclist: [
            [{
                "image": "https://xs.23sk.com/files/article/image/57/57829/57829s.jpg",
                "name": "平平无奇大师兄",
                "id": 26,
                "category": 1
            },{
                "image": "https://xs.23sk.com/files/article/image/44/44710/44710s.jpg",
                "name": "大道朝天",
                "id": 0,
                "category": 1
            }, {
                "image": "https://xs.23sk.com/files/article/image/57/57393/57393s.jpg",
                "name": "临渊行",
                "id": 1,
                "category": 1
            }, {
                "image": "https://xs.23sk.com/files/article/image/22/22295/22295s.jpg",
                "name": "逆天邪神",
                "id": 3,
                "category": 1
            }],
            [{
                "image": "https://xs.23sk.com/files/article/image/57/57829/57829s.jpg",
                "name": "平平无奇大师兄",
                "id": 26,
                "category": 1
            },  {
                "image": "https://xs.23sk.com/files/article/image/43/43362/43362s.jpg",
                "name": "轮回乐园",
                "id": 27,
                "category": 1
            }, {
                "image": "https://xs.23sk.com/files/article/image/60/60011/60011s.jpg",
                "name": "三国之曹家逆子",
                "id": 28,
                "category": 1
            }]
        ]
    },
    upper(e) {
        console.log(e)
    },
    lower(e) {
        console.log(e)
    },
    scroll(e) {

        console.log(e);
    },
    tosearch() {
      wx.navigateTo({
        url: '../search/search',
      })
    },
 
    tapMove(e) {
        this.setData({
            scrollTop: this.data.scrollTop + 10
        })
    },
    

    onLoad:async function (options) {
        const that = this;
        //获取屏幕高度
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windowHeight: res.windowHeight
                })
            }
        });

        // 获取swiper信息
        const data = await wx.cloud.callFunction({
            name: 'getBooks',
          })
          this.setData({swiperList:data.result.books.data[0].swiper})
        //   console.log(data.result.books.data[0].swiper)
        

    },
    toList:function() {
        wx.switchTab({
          url: '/pages/classify/index',
        })
    },
    
    //点击事件处理
    clickDetail: function (e) {
        console.log(e)
        wx.navigateTo({
            url: `../detail/detail?id=${e.currentTarget.dataset.id}&category=${e.currentTarget.dataset.category}&title=${e.currentTarget.dataset.title}`
        });
    },
    onReady: function () {
        // 页面渲染完成
    },
    onShow:async function () {
        // 页面显示
        console.log('---------------------')
        let arr = [];

        const data = await wx.getStorage({
            key:'user'
        })
        data.data.bookshelf.forEach(item => {
            let {id,author,image,desc,lastUpdata,lastchpter,name,state} = item;
            const data = {id,author,image,desc,lastUpdata,lastchpter,name,state}
            arr.push(data)
        })
        let {token,nickName,avatarUrl,loading} = data.data
        const userInfo = {token,nickName,avatarUrl,loading}
        // 将数据传到后台，用于更新数据库
        this.setData({openid:data.data.openid,bookshelf:arr})

        // 传入数据，调用方法修改数据库
        wx.cloud.callFunction({
            name: 'upDataBookshelf',
            data:{openid:this.data.openid,bookshelf:this.data.bookshelf,userInfo}
          })
        .then(res => {
            // console.log('---------------------')
            // console.log(res)
        }) 
        
    },
    onHide: function () {
        // 页面隐藏
    },
    onUnload: function () {
        // 页面关闭
    },
    
})