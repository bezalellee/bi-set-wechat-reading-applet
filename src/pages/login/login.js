const util = require('../../utils/util.js')
const config = require('../../config.js')
Page({
  data: {
    loading: false,
    loadingWechat: false,
    bookshelf:[],
    userData:{},
    about: config.about,
    redirect: encodeURIComponent(`/pages/me/me`),
    title:'登录',
    userInfo: {},
    hasUserInfo: false,
    canIUseGetUserProfile: false,
  },

// 获取用户手机号
// 非个人开发者才能获取到手机信息
handleGetPhoneNumber: function(event) {
    // console.log(event)
},
  onLoad: function(option) {
    // console.log('******************************************')

    // if (config.debug) console.log(option)
    if (option.redirect) {
      this.setData({
        redirect: option.redirect,
      })
    }
  },
  // 获取用户信息（头像、昵称等）
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        // console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  // 获取token
  onShow: function() {
    let token = util.getToken()
    // if (token) {
    //   wx.switchTab({
    //     url: decodeURIComponent(this.data.redirect),
    //   })
    // }
    // return
  },
  // 跳转注册页面
  toRegist:function(e) {
    wx.navigateTo({
      url: '/pages/regist/index',
    })
  },
  // 用户名密码登录
  formSubmit:async function(e) {

    // if (config.debug) console.log("formSubmit", e);
    // if (this.data.loading) return;
    if (e.detail.value.password == '' || e.detail.value.username == '') {
      util.toastError('账号和密码均不能为空')
      return
    }

    this.setData({
      loading: true
    })

    let that = this
    const username = e.detail.value.username;
    const password = e.detail.value.password;

      const res = await util.request({username,password})
      let user = res.result.returnValue
      console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-')
      console.log(user)
      // if (user == undefined || user.uid <= 0 || user.token == '') {
      if (user.username == undefined || user.password == undefined) {
        util.toastError('登录失败：未知错误')
        that.setData({
          loading: false
        })
        return
      }
      util.setUser(user)
      util.toastSuccess('登录成功')

      // 登陆成功后将状态和信息保存到本地
      wx.getStorage({
        key:'booklist',
        success(res) {
          that.data.bookshelf = res.data
        }
      })

      wx.getStorage({
        key: 'token',
        success (res) {
          user.token = res.data
          // 将token保存到本地
          wx.setStorage({
            key:'user',
            data:user
          })
        }
      })

      wx.getStorage({
        key: 'user',
        success (result) {
          let userData = JSON.parse(result.data)
          userData.appid = user.appid
          userData.openid = user.openid
          userData.loading = true
          userData.bookshelf = that.data.bookshelf
          that.setData({userData})
          // wx.removeStorage({
          //   key: 'user',
          //   success (res) {
          //     console.log(res,'///////////')
          //   }
          // })
          // 将更新数据后的user保存到本地
          wx.setStorage({
            key:'user',
            data:userData
          })
        }
      })

      setTimeout(function() {
        util.redirect(decodeURIComponent(that.data.redirect))
      }, 1500)
  },
  // 找回密码
  findPassword: function(e) {
    wx.showModal({
      title: '温馨提示',
      content: '目前小程序暂不支持找回密码的功能，',
    })
  },
  // 微信登录
  // wechatLogin: function(e) {
  //   let that = this
  //   let weUser = e.detail

  //   if (that.data.loadingWechat) return
  //   that.setData({loadingWechat: true})
    
  //   wx.login({
  //     success(res) {
  //       // if (config.debug) console.log("微信登录", res, weUser)
  //       if (res.code) {
  //         // util.request(config.api.loginByWechat, {code: res.code,userInfo: weUser.rawData,}, 'POST')
  //         util.request()
  //         .then(function(res) { // 登录成功
  //           console.log(res)
  //           let user = res.data.user
  //           if (user == undefined || user.uid <= 0 || user.token == '') {
  //             util.toastError('登录失败：未知错误')
  //             that.setData({
  //               loadingWechat: false
  //             })
  //             return
  //           }
  //           util.setUser(user)
  //           util.toastSuccess('登录成功')
  //           setTimeout(function() {
  //             util.redirect(decodeURIComponent(that.data.redirect))
  //           }, 1500)
  //         })
  //         // .catch(function(e) { // 如果是 401，则跳转到信息绑定页面，否则直接提示相关错误信息
  //         //   // if (config.debug) console.log(e)
  //         //   if (e.statusCode == 401) {
  //         //     getApp().globalData.wechatUser = weUser
  //         //     wx.navigateTo({
  //         //       url: '/pages/bind/bind?redirect=' + that.data.redirect + "&sess=" + encodeURIComponent(e.data.data.sess),
  //         //     })
  //         //   } else {
  //         //     util.toastError(e.data.message || e.errMsg)
  //         //   }
  //         //   that.setData({
  //         //     loadingWechat: false
  //         //   })
  //         // })
  //       } else {
  //         util.toastError('登录失败！' + res.errMsg)
  //       }
  //     },
  //     fail: function(e) {
  //       util.toastError(e.errMsg)
  //     }
  //   })
  // }
})