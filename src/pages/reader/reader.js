var app = getApp()
var https = require('../../utils/https.js')
var util = require('../../utils/util.js')
var moveTime = null; //控制左滑右滑的动画
var isMoving = 0;
var leftTimmerCount = 0;
var rightTimmerCount = 0;
var hasRunTouchMove = false;

/**
 * @param str 需要分页的内容
 * @param fontSize 当前的字体大小
 * @param lineHeight 当前的行高
 * @param windowW 当前window的宽度
 * @param windowH 当前window的高度
 * @param pixelRatio 当前分辨率，用来将rpx转换成px
 */

function countPageNum(str, fontSize, lineHeight, windowW, windowH, pixelRatio) {
    var returnNum = 0;
    fontSize = fontSize;
    lineHeight = lineHeight;
    //将str根据’\n‘截成数组
    var strArray = str.split(/\n+/);
    var splitArray = [];//换行符的个数集合
    var reg = new RegExp('\n+', 'igm');
    var result = '';
    //这里写一个for循环去记录每处分隔符的\n的个数，这将会影响到计算换行的高度
    while ((result = reg.exec(str)) != null) {
        splitArray.push(result.toString().match(/\n/img).length);
    }
    var totalHeight = 0;
    strArray.forEach(function (item, index) {
        var wrapNum = 0;
        //splitArray的长度比strArray小1
        if (splitArray.length < index) {
            wrapNum = splitArray[index] - 1;
        }
        //Math.ceil向上取整
        totalHeight += Math.ceil(item.length / Math.floor((windowW - 120 / pixelRatio) / (fontSize/pixelRatio))) * lineHeight + wrapNum * lineHeight;

    });
    return Math.ceil(totalHeight / (windowH)) + 1;
}

function gotoReader(id, serialNumber,title) {
//    var that = this;
    wx.redirectTo({
        url: `../reader/reader?id=${id}&serialNumber=${serialNumber+1}&title=${title}`
    });
}

Page({
    data: {
        id: '',
        history:[],
        size:0,
        fontColor:'',
        menuBg:'',
        pageBg:'',
        show:false,
        serialNumber: 0,
        fontSize:0,
        zhutiList:['rgb(204,232,207)','#121212','rgb(194,177,150)'],
        dqzhuti:0,
        keepeyes:0,
        autoRead:false,
        isShow:true,
        res:{},
        title: '',
        Name: '',
        content: '',
        showCatelog: false,
        catelogs: {},
        fontSize: 36, //单位rpx
        lineHeight: 26, //单位rpx
        windows: {windowsHeight: 0, windowsWidth: 0, pixelRatio: 1,pageHeight:0},
        touches: {lastX: 0, lastY: 0},
        moveDirection: 0, //0代表左滑动，1代表右滑动
        leftValue: 0,
        pageIndex: 1,
        pageNum: 0,
        preItemId: 0,
        nextItemId: 0,
        isShowPlayer:false,
        isPlayer:false,
        rotate:0,
        count:0,
        audioList:[],
        audioContext:{},
        aduioName:'',
        currentPlay:0,
        volume:50, //音量，
        isloop:false, //是否循环播放
        progress:0, //播放进度
        totalTime:0, //播放总时间 
        currentTime:0 ,//当前播放时间

    },
     // 背景音乐设置
    setVolume:function(e){
        this.setData({volume:e.detail.value})
        this.data.audioContext.volume = this.data.volume/100;
    },
    // 播放进度
    // setProgress:function(e){
    //     this.setData({currentTime:e.detail.value})
    //     this.data.audioContext.currentTime = this.data.progress*(this.data.audioContext.currentTime/this.data.audioContext.duration);
    // },
    // 播放器
    openList:async function(){
        const that = this;
        // let timer;
        this.setData({isShowPlayer:!this.data.isShowPlayer})
        // if(isPlayer){
        //     timer = setInterval(() =>{
        //         that.setData({rotate:this.data.rotate+.01})
        //     },500)
        // }else {
        //     clearInterval(timer)
        // }

        // 获取歌曲信息
        // 第一次点击获取信息
        if(this.data.count == 0){
            this.data.count++;

            // 创建音乐播放器实例
            this.createAudio();
            console.log('this is ok')

            wx.cloud.callFunction({
                name:'player'
            }).then(res =>{
                // console.log(res.result.data.data,'--------------')
                that.setData({audioList:res.result.data.data})
            })
        }

         
    },
    // 创建一个播放器实例
    createAudio:function(){
        // 创建播放器
        const audioContext =wx.createInnerAudioContext();
        this.setData({audioContext})
        return audioContext
    },
    playing:function() {
        this.setData({isPlayer:!this.data.isPlayer})

        this.data.audioContext.src = this.data.audioList[this.data.currentPlay].audioUrl
        this.setData({aduioName:this.data.audioList[this.data.currentPlay].name})
        // 播放
        // 直接调用play方法播放
        // 会直接播放
        if(this.data.isPlayer){
            this.data.audioContext.play();
        }else {
            // 暂停
            this.data.audioContext.stop()
        }
        this.setTimeToPlay(this.data.isPlayer);

        
    },
    // 设置播放时间显示
    setTimeToPlay:function(isPlayer){
        let timer;
        // 设置总时间
        // 转换为分钟
        const date = this.data.audioContext.duration;
        const hour = parseInt(date/3600);
        const minute = parseInt(date/60);
        const second = Math.ceil(date%60);
        // 组合为要显示的时间
        const times =hour + ':' + minute + ':' + second ;
        // console.log(times)
        this.setData({totalTime:times})
         // 设置当前时间
         const current = this.data.audioContext.currentTime
         this.setData({currentTime:Math.ceil(current)})

         let pros;
        if(isPlayer) {
            timer = setInterval((res)=>{
                // 设置当前时间
                const current = this.data.audioContext.currentTime;
                this.setData({currentTime:Math.ceil(current)})
                pros = Math.ceil(current/date*100)
                // 设置进度条 
                this.setData({progress:pros})
            },500)
        }else {
            clearInterval(timer)
        }
        
        
    },
    nextAudio:function(){
        // console.log(this.data.currentPlay)
        if(this.data.currentPlay >= this.data.audioList.length -1){
            this.setData({currentPlay:0})
        }else {
            this.setData({currentPlay:this.data.currentPlay + 1})
        }

        this.data.audioContext.src = this.data.audioList[this.data.currentPlay].audioUrl
        this.setData({aduioName:this.data.audioList[this.data.currentPlay].name})

        this.setData({isPlayer:true})
        this.setTimeToPlay(this.data.isPlayer);
        this.data.audioContext.play();
        
    },
    preAudio:function(){
        console.log(this.data.currentPlay)
        if(this.data.currentPlay <= 0){
            this.setData({currentPlay:this.data.audioList.length -1})
        }else {
            this.setData({currentPlay:this.data.currentPlay - 1})
        }

        this.data.audioContext.src = this.data.audioList[this.data.currentPlay].audioUrl
        this.setData({aduioName:this.data.audioList[this.data.currentPlay].name})

        this.setData({isPlayer:true})
        this.setTimeToPlay(this.data.isPlayer);
        this.data.audioContext.play();
        
    },

    // 
    onReady: function () {
        var that = this;
        //获取屏幕的高度和宽度，为分栏做准备 
        wx.getSystemInfo({
            success: function (res) {
                that.setData({
                    windows: {
                        windowsHeight: res.screenHeight,
                        pageHeight: res.windowHeight,
                        windowsWidth: res.windowWidth,
                        pixelRatio: res.pixelRatio
                    }
                });
            }
        });
    },
    openOrcloseSet:function(){
        this.setData({isShow:!this.data.isShow})
    },
    slider2change:function(e){
        this.setData({fontSize:e.detail.value,menuBg:e.currentTarget.dataset.backcolor})
    },
    slider4change:function(e){

        if(e.currentTarget.dataset.dqzhuti ==0) {
            this.setData({dqzhuti:1,menuBg:'#121212'})
        }else {
            this.setData({dqzhuti:0,keepeyes:0,menuBg:'rgb(194,177,150)'})
        }
        
    },
    slider3change:function(e) {
        if(e.currentTarget.dataset.keepeyes !== 'rgb(204,232,207)'){
            this.setData({menuBg:'rgb(204,232,207)'})
        }else {
            this.setData({dqzhuti:0,menuBg:'rgb(194,177,150)'})
        }
    },
    
    toReader: function (e) {
        var that = this;
        let serialNumber = 1;
        if (e.currentTarget.dataset.serialnumber) {
            serialNumber = parseInt(e.currentTarget.dataset.serialnumber);
        }
      var title = e.currentTarget.dataset.title;
      if (!title) {
        title = that.data.title;
      }
    
      gotoReader(that.data.id, serialNumber, title);
    },
    changeShowCatelog: function (e) {
        var that = this;
        that.setData({
            showCatelog: !that.data.showCatelog
        })
        // if (that.data.showCatelog) {
        //     that.initCatelog(that.data.id);
        // }
        if (that.data.showCatelog) {
            that.setData({
                catelogs: that.data.res,
                pageindex : 1
            })
        }
    },

    initChapters: async function (id, serialNumber, loadPageIndex) {
        
        var that = this;
        let url = id;
        wx.showLoading({
            title: '加载中',
            mark: true
        });
        var pageindex = 1;
        if (loadPageIndex) {
            pageindex = that.data.pageIndex;
        }

        const data = await wx.cloud.callFunction({
            name: 'getBooks',
            
          })
        //   const snb = (serialNumber-serialNumber%10) /10
          const snb = serialNumber
          let content ='';
          let name = '';
          let bookname = '';
          data.result.books.data[0].booklist.forEach(item => {
            //   if(item.id == 0) {
            //       content = item.
            //   }
              if(item.id == id){
                that.setData({res:item})
              }
              bookname = item.name;
              item.text.forEach(it =>{
                //   content = it.content;
                  if(it.chapterId == snb){
                    content = it.content;
                    name = it.name
                  }
              })
          })

            try {
              content = content.trim();
              name = (name || '').trim();
              var nextItemId = snb+1;
              var preItemId = snb-1;
              var id = snb;
              
              content = content.replace(/<br\s*\/*>/g, "\n");
              content = content.replace(/&nbsp;/g, " ");
              content = content.replace("</a></p>", "");
              content = content.replace("</a></p>", "");
              content = content.replace("<div id=\"content\">", "");
              content = content.replace("</div>", "");
              if (content.match(/<p style(.*)/)) {
                var footP = content.match(/<p style(.*)/)[0]
                if (footP) {
                  content = content.replace(footP, "");
                }

              }

                var pageNum = countPageNum(content, that.data.fontSize, that.data.lineHeight, that.data.windows.windowsWidth, that.data.windows.windowsHeight, that.data.windows.pixelRatio);
                var windth = that.data.windows.windowsWidth;
                var leftValue = windth * (pageindex - 1)
                //重新排版
                that.setData({
                    content: content,
                    name: name,
                    nextItemId: nextItemId,
                    preItemId: preItemId,
                    serialNumber: id,
                    pageNum: pageNum,
                    pageIndex: pageindex,
                    leftValue: -leftValue,
                    moveDirection: 0,
                    touches: {lastX: 0, lastY: 0}
                });

                let history = [];
                 // 先获取已有的历史记录
                wx.getStorage({
                    key:'history',
                    success(res){
                        // 将数据保存
                        history = res.data;
                        history.push(bookname)
                        // 将书籍添加到历史记录
                        wx.setStorage({
                            key:'history',
                            data:history
                        })
                    }
                })


                
                wx.hideLoading();
            } catch (e) {
                console.log(e);
                wx.hideLoading();
            }
            wx.hideLoading();
    },


    refreshCatelog: function () {
        var that = this;
        that.initCatelog(that.data.id);
    },
    sortCatelog: function () {
        var that = this;
        var catelogs = that.data.catelogs;
        catelogs.text = catelogs.text.reverse();
        that.setData({
            catelogs: catelogs
        });
    },
    initCatelog: function (id) {
      var that = this;

      wx.showLoading({
        title: '加载中',
        mark: true
      });
        
    },
    onLoad: function (options) {
        
        
        this.setData({size:36})
      if (options.title){
        wx.setNavigationBarTitle({
          title: options.title
        });
      }
        var that = this;
        var serialNumber = 1;//目录号
        var pageIndex = 1;
        if (!options.serialNumber) {
            var serail = that.getReadProgress(options.id);
            
            serialNumber = serail.split("_")[0] || 1;
            pageIndex = serail.split("_")[1] || 1;
        } else {
            serialNumber = parseInt(options.serialNumber)
        }
        

        showCatelog: (options.showCatelog == "true" ? true : false)
        //动态设置标题
        that.setData({id: options.id, serialNumber: serialNumber, title: options.title, pageIndex: pageIndex});
        that.initChapters(options.id, serialNumber, true);


       
    },

    onUnload: function () {
        // 页面关闭，存储页面进度
        this.setReadProgress();
        console.log("reading onUnload");
    },

    //重新显示页面执行函数
    onShow: function () {
        var that = this;
        //读取用户设置
        wx.getStorage({
            key: 'readerSetting',
            success: function (res) {
                var userSetting = JSON.parse(res.data);
                that.setData({
                    fontSize: userSetting.fontSize || that.data.fontSize,
                    pageIndex: userSetting.pageIndex || that.data.pageIndex,
                });
            }
        });

        // 监听音乐播放
        
        // let that = this
        // wx.getBackgroundAudioManager(() => {
        // that.timer && clearInterval(that.timer)
        // that.timer = setInterval(() => {
        //     wx.getBackgroundAudioPlayerState({
        //     success: res => {
        //         let per = (res.currentPosition/res.duration)*10000
        //         that.setData({
        //         musicPercent: Math.round(per)/100 + '',
        //         duration: res.duration
        //         })
        //     }
        //     })
        // }, 1000)
        // })

        // // 监听背景音频暂停事件
        // wx.onBackgroundAudioPause(() => {
        // clearInterval(that.timer)
        // })

        // // 监听背景音频停止事件
        // wx.onBackgroundAudioStop(() => {
        // clearInterval(that.timer)
        // })
    },
    //跳出页面执行函数
    onHide: function () {
        var that = this;
        //onUnload方法在页面被关闭时触发，我们需要将用户的当前设置存下来
        try {
            var userSetting = {
                fontSize: that.data.fontSize, // 控制当前章节，亮度，字体大小
                pageIndex: that.data.pageIndex, // 当前第几页
            };
            wx.setStorage('readerSetting', JSON.stringify(userSetting));
        } catch (e) {
            console.log(e);
        }
    },
    handleTouchMove: function (e) {
        var that = this;
        if (isMoving == 1) {
            return;
        }
        var currentX = e.touches[0].pageX;
        var currentY = e.touches[0].pageY;
        // 判断没有滑动而是点击屏幕的动作
        hasRunTouchMove = true;
        console.log('正在执行touchmove, isMoving为：' + isMoving + '------e: {x: ' + e.touches[0].pageX + ' ,y: ' + e.touches[0].pageY + '}');
        var direction = 0;
        if ((currentX - that.data.touches.lastX) < 0) {
            direction = 0;
        }
        else if (((currentX - that.data.touches.lastX) > 0)) {
            direction = 1;
        }
        //需要减少或者增加的值
        //将当前坐标进行保存以进行下一次计算
        that.setData({touches: {lastX: currentX, lastY: currentY}, moveDirection: direction});
    },
    handleTouchStart: function (e) {
        var that = this;
        // 判断用户的点击事件，如果不是滑动，将不会执行touchmove
        hasRunTouchMove = false;
        var width = that.data.windows.windowsWidth;
        var direction = 0;
        if (e.touches[0].pageX < (that.data.windows.windowsWidth / 2)) {
            direction = 1;
        }
        if (isMoving == 0) {
            that.setData({touches: {lastX: e.touches[0].pageX, lastY: e.touches[0].pageY}, moveDirection: direction});
        }
    },
    getReadProgress(id) {
        var key = "ReadProgress_" + id;
        var rtn = wx.getStorageSync(key);
        if (rtn == "_undefined") {
            rtn = "1_1";
        }
        return rtn;
    },
    setReadProgress(e) {
        var that = this;
        var key = "ReadProgress_" + that.data.id;
        var data = that.data.serialNumber + "_" + that.data.pageIndex;
        wx.setStorage({
            key: key,
            data: data
        });
    },
    autoReads:function(e){
        const that = this;
        that.setData({autoRead:!this.data.autoRead})
        if(!this.data.autoRead) {
            clearInterval(timer)
        }
        const timer = setInterval(() => {
            if(!this.data.autoRead) {
                clearInterval(timer)
            }
            if(that.data.pageIndex == that.data.pageNum) {
                that.handleTouchEnd(e,1);
            }else {
                that.handleTouchEnd(e);
            }
        },1000)
        if(!this.data.autoRead) {
            clearInterval(timer)
        }
        
    },
    handleTouchEnd: function (e,index = 3) {
        console.log('正在执行touchend, isMoving为：' + isMoving);
        var that = this;
        // const snb = (serialNumber-serialNumber%10) /10
        // 判断用户的点击事件，决定是否显示控制栏
        if (hasRunTouchMove == false) {
            var y = that.data.touches.lastY;
            var x = that.data.touches.lastX;
            var h = that.data.windows.windowsHeight / 2;
            var w = that.data.windows.windowsWidth / 2;
            if (x && y && y >= (h - 50) && y <= (h + 50) && x >= (w - 60) && x <= (w + 60)) {
                that.changeShowCatelog();
                return;
            }
        }
        //左滑动和有滑动的操作
        var currentIndex = that.data.pageIndex; //当前页数
        
        if(e.currentTarget.dataset.changepage == 1 || index ==1) {
            currentIndex = that.data.pageNum
        }

        if(e.currentTarget.dataset.changepage == 2) {
            that.initChapters(that.data.id, that.data.preItemId)
        }
        if (isMoving == 0) {
            if (that.data.moveDirection == 0) {
                if (currentIndex < that.data.pageNum) {
                    isMoving = 1;
                    var windth = that.data.windows.windowsWidth;
                    ++currentIndex;
                    var leftValue = windth * (currentIndex - 1)
                    that.setData({pageIndex: currentIndex, leftValue: -leftValue});
                } else {
                    that.initChapters(that.data.id, that.data.nextItemId);
                    console.log(that.data.nextItemId)
                    console.log("下一页");
                }
            } else {
                //前一页和后一页相差其实是2个-320px
                if (currentIndex > 1) {
                    isMoving = 1;
                    var windth = that.data.windows.windowsWidth;
                    --currentIndex;
                    var leftValue = windth * (currentIndex - 1)
                    that.setData({pageIndex: currentIndex, leftValue: -leftValue});
                }
                else {
                    var serialNumber = that.data.serialNumber;
                    if (serialNumber <= 1) {
                        wx.showToast({
                            title: '亲,已经是第一章了:）',
                            icon: 'success',
                            duration: 2000
                        })
                    } else {
                        that.initChapters(that.data.id, that.data.preItemId);
                    }
                    console.log("上一页");
                }
            }
            isMoving = 0;
        } else {

        }
    }
});