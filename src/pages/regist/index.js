const util = require('../../utils/util.js')
const config = require('../../config.js')
Page({
  data: {
    userData:{},
    // redirect: encodeURIComponent(`/pages/login/login`),
    title:'注册',
  },

  onLoad: function(option) {

    if (option.redirect) {
      this.setData({
        redirect: option.redirect,
      })
    }
  },

  onShow: function() {
    
  },
  // 用户名密码登录
//   通过给表单绑定一个函数，通过函数的event对象获取表单输入值
  formSubmit:async function(e) {
    if (e.detail.value.password == '' || e.detail.value.username == '') {
      util.toastError('账号和密码均不能为空')
      return
    }

    const username = e.detail.value.username;
    const password = e.detail.value.password;

    //   调用注册云函数
    const isReg = await wx.cloud.callFunction({
        name:"addUser",
        data:{username,password}
    })
    if(isReg.result.isReg) {
      util.toastSuccess('注册成功')
      setTimeout(function() {
        wx.navigateTo({
          url: '/pages/login/login',
        })
      }, 1500)
    }else {
      util.toastError('注册失败：未知错误')
      return;
    }
  },
})