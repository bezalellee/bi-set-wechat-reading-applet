// pages/set/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        size:0,
        fontColor:'',
        menuBg:'',
        show:false,
        serialNumber: 0,
        fontSize:0,
        zhutiList:['rgb(204,232,207)','#121212','rgb(194,177,150)'],
        dqzhuti:0,
        keepeyes:0,
        isShow:true,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },
    slider2change:function(e){
        this.setData({fontSize:e.detail.value,menuBg:e.currentTarget.dataset.backcolor})
    },
    slider4change:function(e){

        if(e.currentTarget.dataset.dqzhuti ==0) {
            this.setData({dqzhuti:1,menuBg:'#121212'})
        }else {
            this.setData({dqzhuti:0,keepeyes:0,menuBg:'rgb(194,177,150)'})
        }
        
    },
    slider3change:function(e) {
        if(e.currentTarget.dataset.keepeyes !== 'rgb(204,232,207)'){
            this.setData({menuBg:'rgb(204,232,207)'})
        }else {
            this.setData({dqzhuti:0,menuBg:'rgb(194,177,150)'})
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})