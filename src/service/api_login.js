import { hyLoginRequest } from './index'
export function getLoginCode() {
    return new Promise((resolve,reject) =>{
      wx.login({
      // 设置超时使劲按
      timeout:1000,
      // 成功时拿到code
      success:res=>{
        // console.log(res)
        // 拿到code
        const code = res.code;
        resolve(code)

        // 发送网络请求，发送给服务器（很容易产生回调地狱）
      },
      // 失败时的信息
      fail:err =>{
        // console.log(err)
        // 
        reject(err)
      }
    })
    })
}  

// 将code发送给服务器
export function codeToToken(code) {
    // 将发送给服务器
    return hyLoginRequest.post("/login",{code})
}

// token
export function checkToken(){
  return hyLoginRequest.post("/auth",{},true)
}

// session
export function checkSession() {
  return new Promise((resolve) =>{
    // 判断session有没有过期
    wx.checkSession({
      success: (res) => {
        // 没有过期
        resolve(true)
      },
      fail:() =>{
        resolve(false)
      }
    })
  })
}

// 封装获取用户信息函数
export function getUserInfo() {
  return new Promise((resolve,reject) =>{
    wx.getUserProfile({
      desc: 'desc',
      success:(res) =>{
        resolve(res)
      },
      fail:(err) =>{
        reject(err)
      }
    })
  })
}