const data = {
  "_id": "01d95bfe-ec2d-4842-82d1-152ea97aa95b",
  "order": 20.0,
  "pageSize": 10.0,
  "url": "www.23txt.com",
  "name": "天籁小说网",
  "enable": true,
  "search": {
    "url": "https://www.23txt.com/search.php?",
    "searchData": {
      "key": "keyword",
      "extraData": {}
    },
    "result": {
      "data": {
        "selector": "div.result-list\u003ediv.result-item.result-game-item",
        "type": "array",
        "result": {
          "update_time": {
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "function": "find",
              "args": "div.result-game-item-detail\u003ediv.result-game-item-info\u003ep.result-game-item-info-tag\u003espan.result-game-item-info-tag-title"
            }, {
              "function": "eq",
              "args": 4.0,
              "type": "function"
            }, {
              "function": "text",
              "type": "function"
            }]
          },
          "name": {
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "function": "find",
              "args": "div.result-game-item-detail\u003eh3.result-item-title.result-game-item-title\u003ea.result-game-item-title-link"
            }, {
              "type": "function",
              "function": "attr",
              "args": "title"
            }, {
              "function": "trim",
              "type": "function"
            }]
          },
          "cover": {
            "attr": "src",
            "type": "selector",
            "selector_handle": [{
              "function": "find",
              "args": "div.result-game-item-pic\u003ea.result-game-item-pic-link\u003eimg.result-game-item-pic-link-img",
              "type": "function"
            }]
          },
          "author": {
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "args": "div.result-game-item-detail\u003ediv.result-game-item-info\u003ep.result-game-item-info-tag\u003espan",
              "function": "find"
            }, {
              "args": 1.0,
              "function": "eq",
              "type": "function"
            }, {
              "type": "function",
              "function": "text"
            }, {
              "function": "trim",
              "type": "function"
            }]
          },
          "description": {
            "selector_handle": [{
              "function": "find",
              "args": "div.result-game-item-detail\u003ep.result-game-item-desc",
              "type": "function"
            }, {
              "type": "function",
              "function": "text"
            }],
            "type": "selector"
          },
          "category": {
            "type": "selector",
            "selector_handle": [{
              "args": "div.result-game-item-detail\u003ediv.result-game-item-info\u003ep.result-game-item-info-tag\u003e span.result-game-item-info-tag-title",
              "function": "find",
              "type": "function"
            }, {
              "function": "eq",
              "type": "function",
              "args": 2.0
            }, {
              "type": "function",
              "function": "text"
            }]
          },
          "latest_chapter_name": {
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "function": "find",
              "args": "div.result-game-item-detail\u003ediv.result-game-item-info\u003ep.result-game-item-info-tag\u003ea.result-game-item-info-tag-item"
            }, {
              "type": "function",
              "function": "text"
            }]
          },
          "latest_chapter_url": {
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "function": "find",
              "args": "div.result-game-item-detail\u003ediv.result-game-item-info\u003ep.result-game-item-info-tag\u003ea.result-game-item-info-tag-item"
            }],
            "attr": "href"
          },
          "read_url": {
            "attr": "href",
            "type": "selector",
            "selector_handle": [{
              "type": "function",
              "function": "find",
              "args": "div.result-game-item-pic\u003ea.result-game-item-pic-link"
            }]
          }
        }
      }
    }
  },
  "detail": {
    "type": "gbk",
    "result": {
      "category": {
        "selector": "meta[property=\"og:novel:category\"]",
        "attr": "content"
      },
      "read_url": {
        "selector": "meta[property=\"og:novel:read_url\"]",
        "attr": "content"
      },
      "latest_chapter_name": {
        "selector": "meta[property=\"og:novel:latest_chapter_name\"]",
        "attr": "content"
      },
      "description": {
        "selector": "meta[property=\"og:description\"]",
        "attr": "content"
      },
      "update_time": {
        "selector": "meta[property=\"og:novel:update_time\"]",
        "attr": "content"
      },
      "latest_chapter_url": {
        "attr": "content",
        "selector": "meta[property=\"og:novel:latest_chapter_url\"]"
      },
      "status": {
        "selector": "meta[property=\"og:novel:status\"]",
        "attr": "content"
      },
      "cover": {
        "selector": "meta[property=\"og:image\"]",
        "attr": "content"
      },
      "first_chapter_url": {
        "selector": "div#list\u003edl\u003edd\u003ea",
        "selector_handle": [{
          "type": "function",
          "function": "eq",
          "args": "0"
        }],
        "attr": "href",
        "baseUrl": "https://www.23txt.com",
        "type": "selector"
      },
      "author": {
        "selector": "meta[property=\"og:novel:author\"]",
        "attr": "content"
      },
      "name": {
        "attr": "content",
        "selector": "meta[property=\"og:novel:book_name\"]"
      }
    }
  },
  "catalogs": {
    "type": "gbk",
    "result": {
      "name": {
        "selector": "meta[property=\"og:novel:book_name\"]",
        "attr": "content"
      },
      "catalogs": {
        "selector": "div#list\u003edl\u003edd\u003ea",
        "result": {
          "name": {
            "function": "text",
            "type": "function"
          },
          "url": {
            "baseUrl": "https://www.23txt.com",
            "attr": "href"
          }
        },
        "type": "array"
      }
    }
  },
  "content": {
    "type": "gbk",
    "result": {
      "content": {
        "selector": "div#content",
        "type": "function",
        "function": "html",
        "br2n": true
      },
      "name": {
        "function": "text",
        "selector": "div#wrapper\u003ediv.content_read\u003ediv.box_con\u003ediv.bookname\u003eh1",
        "type": "function"
      },
      "next": {
        "selector": "div#wrapper\u003ediv.content_read\u003ediv.box_con\u003ediv.bottem2\u003ea",
        "attr": "href",
        "baseUrl": "https://www.23txt.com",
        "type": "selector",
        "selector_handle": [{
          "type": "function",
          "function": "eq",
          "args": 2.0
        }]
      },
      "prev": {
        "baseUrl": "https://www.23txt.com",
        "selector_handle": [{
          "type": "function",
          "function": "eq",
          "args": 0.0
        }],
        "selector": "div#wrapper\u003ediv.content_read\u003ediv.box_con\u003ediv.bottem2\u003ea",
        "type": "selector",
        "attr": "href"
      }
    }
  }
}


module.exports = {
  data: data
}