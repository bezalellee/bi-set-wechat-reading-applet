// import { HYEventStore  } from "hy-event-store";
// import fetch from "../common/request.js";

// const store = new HYEventStore({
//   ctx: {
//     version: "2.0.0",
//     isLogin: false,
//     bookshelfs: [],
//     openid: "",
//     userInfo: {
//       // 用户信息
//       longitude: null, // 经度
//       latitude: null, // 纬度
//       locationCity: "" // 定位城市
//     },
//     config: {
//       // 背景颜色
//       bgColor: "#D5EED1",
//       // 字体颜色
//       color: "#282D29",
//       // 是否夜间模式
//       isNight: false,
//       // 亮度
//       brightness: 0,
//       // 是否跟随系统亮度
//       followSysBright: true,
//       // 系统亮度
//       sysBright: 0,
//       // 字体大小
//       fontSize: 40,
//       // 行间距
//       lineHeight: 56,
//       // 常亮
//       sleep: 5,
//       // 翻页动画
//       animate: "none"
//     },

//     mobile: wx.getStorageSync("thorui_mobile") || "echo.",
//     memberId: wx.getStorageSync("memberId") || 0,
//     appInfo: {
//       // APP整体数据
//       commonDataVersion: "0", // 公共数据的大版本号
//       adVersion: "0", // 广告数据版本号
//       serviceVersion: "0" // 服务数据版本号
//     },
//     adData: {
//       homePageAdverts: [],
//       carPageAdverts: [],
//       servicePageAdverts: []
//     },
//     modulesData: {
//       // 首页服务模块、产品模块和服务页服务模块数据
//       serviceModules: [],
//       productModules: [],
//       serviceFuncVOList: []
//     }
//   },
  
//   actions: {
//     LOGIN(ctx, payload) {
//       if (payload) {
//         ctx.openid = payload.openid;
//         // ctx.mobile = payload.mobile;
//         // ctx.memberId = payload.memberId;
//       }
//       ctx.isLogin = true;
//     },
//     logout(ctx) {
//       ctx.mobile = "";
//       ctx.memberId = 0;
//       ctx.isLogin = false;
//     },
//     setOpenid(ctx, openid) {
//       ctx.openid = openid;
//     },
//     // 设置用户信息
//     setUserInfo(ctx, payload) {
//       for (let i in payload) {
//         for (let j in ctx.userInfo) {
//           if (i === j) {
//             ctx.userInfo[j] = payload[i];
//           }
//         }
//       }
//     },
//     // 设置APP信息
//     setAppInfo(ctx, payload) {
//       for (let i in payload) {
//         for (let j in ctx.appInfo) {
//           if (i === j) {
//             ctx.appInfo[j] = payload[i];
//           }
//         }
//       }
//     },
//     // 设置配置信息
//     setConfig(ctx, payload) {
//       ctx.config = { ...ctx.config, ...payload };
//     },
//     // 设置书架信息
//     setBookshelfs(ctx, payload) {
//       ctx.bookshelfs = [...payload];
//     },
//     // 更新APP整体广告数据
//     updateAdData(ctx, payload) {
//       wx.setStorageSync(
//         "carPageAdverts",
//         JSON.stringify(payload.adData.carPageAdverts)
//       );
//       ctx.adData = payload.adData;
//     },
//     // 更新APP整体服务数据
//     updateModulesData(ctx, payload) {
//       ctx.modulesData = payload.modulesData;
//     }
//   },


//   async login({ dispatch }) {
//     const res = await wx.cloud.callFunction({
//       name: "login"
//     });
//     const { openid, config } = res.result.data;
//     dispatch("LOGIN", { openid });
//     if (config) {
//       dispatch("setConfig", config);
//     }
//   },
//   // 添加书架
//   addBookshelfs({ dispatch, ctx }, payload) {
//     dispatch("setBookshelfs", [...ctx.bookshelfs, payload]);
//   },
//   // 查看公共数据版本是否更新
//   // 返回有data数据时代表有更新，未返回data数据代表不需要更新
//   // 返回9010错误时代表还未生成公共数据版本，需要先调用对应接口生成数据版本号
//   async checkModuleUpdate({ dispatch, ctx }) {
//     return new Promise((resolve, reject) => {
//       fetch
//         .request(
//           "config/queryHasUpdates",
//           {
//             version: ctx.appInfo.commonDataVersion
//           },
//           "POST"
//         )
//         .then(res => {
//           //console.log('各个模块数据数据' + JSON.stringify(res))
//           if (res.code === 200) {
//             if (!res.data) return;
//             const data = res.data.versionData;
//             let obj = {
//               updateAd: false,
//               updateService: false
//             };
//             dispatch("setAppInfo", {
//               commonDataVersion: res.data.version
//             });
//             if (data.advertVersion !== ctx.appInfo.adVersion) {
//               obj.updateAd = true;
//             }
//             if (data.serviceVersion !== ctx.appInfo.serviceVersion) {
//               obj.updateService = true;
//             }
//             resolve(obj);
//           } else {
//             fetch.toast(res.message);
//           }
//         })
//         .catch(e => { });
//     });
//   },
//   // 查询广告（首页、车圈、服务）
//   getAds({ dispatch, dispatch, ctx }) {
//     fetch
//       .request(
//         "config/queryAdverts",
//         {
//           clientDictKey: 1,
//           version: ctx.appInfo.adVersion
//         },
//         "POST"
//       )
//       .then(res => {
//         if (res.code === 200) {
//           dispatch("updateAdData", {
//             adData: res.data
//           });
//           dispatch("setAppInfo", {
//             adVersion: res.data.version
//           });
//         } else {
//           fetch.toast(res.message);
//         }
//       })
//       .catch(e => { });
//   },
//   // 获取首页服务模块、产品模块和服务页服务模块数据
//   getServices({ dispatch, ctx }) {
//     fetch
//       .request(
//         "serviceFunc/searchServiceFunc",
//         {
//           version: ctx.appInfo.serviceVersion
//         },
//         "POST"
//       )
//       .then(res => {
//         // console.log('首页服务模块、产品模块和服务页服务模块数据' + JSON.stringify(res.data))
//         if (res.code === 200) {
//           dispatch("updateModulesData", {
//             modulesData: res.data.data
//           });
//           dispatch("setAppInfo", {
//             serviceVersion: res.data.version
//           });
//         } else {
//           fetch.toast(res.message);
//         }
//       })
//       .catch(e => { });
//   }

// })

// export {
//   store
// }